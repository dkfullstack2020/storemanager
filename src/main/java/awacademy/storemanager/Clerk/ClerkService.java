package awacademy.storemanager.Clerk;

import awacademy.storemanager.Store.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClerkService {

    private ClerkRepo clerkRepo;

    @Autowired
    public ClerkService(ClerkRepo clerkRepo) {
        this.clerkRepo = clerkRepo;
    }

    public Clerk getClerk(Integer clerkID) {
        Optional<Clerk> clerk = clerkRepo.findById(clerkID);
        return clerk.get();
    }

    public void add(ClerkDTO clerkDTO, Store store) {
        Clerk clerk = new Clerk(clerkDTO.getName(), store);
        clerkRepo.save(clerk);
    }

    public void edit(ClerkDTO clerkDTO, int clerkId) {
        Optional<Clerk> optionalClerk =clerkRepo.findById(clerkId);
        if (optionalClerk.isPresent()){
            Clerk clerk = optionalClerk.get();
            clerk.setName(clerkDTO.getName());
            clerkRepo.save(clerk);
        }
    }


    public void delete(int clerkId) {
        Optional<Clerk> optionalClerk =clerkRepo.findById(clerkId);
        if (optionalClerk.isPresent()){
            clerkRepo.delete(optionalClerk.get());
        }
    }




}
