package awacademy.storemanager.Clerk;

import org.springframework.data.repository.CrudRepository;

public interface ClerkRepo extends CrudRepository<Clerk, Integer> {
}
