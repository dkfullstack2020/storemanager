package awacademy.storemanager.Clerk;

import awacademy.storemanager.Store.Store;

import javax.persistence.*;

@Entity
public class Clerk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne
    private Store store;

    public Clerk(){
    }

    public Clerk(String name, Store store) {
        this.name = name;
        this.store = store;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Store getStore() {
        return store;
    }

    public void setName(String name) {
        this.name = name;
    }
}
