package awacademy.storemanager.Clerk;


import awacademy.storemanager.Store.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.criteria.CriteriaBuilder;

@Controller
public class ClerkController {

    private ClerkService clerkService;
    private StoreService storeService;

    @Autowired
    public ClerkController(ClerkService clerkService, StoreService storeService) {
        this.clerkService = clerkService;
        this.storeService = storeService;
    }

    @GetMapping("/{storeId}/details")
    public String details(Model schaufenster, @PathVariable Integer storeId) {
        schaufenster.addAttribute("clerk", new ClerkDTO(""));
        schaufenster.addAttribute("store", storeService.getStore(storeId));
        return "/details";
    }

    @PostMapping("/{storeId}/details")
    public String addClerk(@ModelAttribute(value = "clerk") ClerkDTO clerkDTO, @PathVariable Integer storeId){
        clerkService.add(clerkDTO, storeService.getStore(storeId));
        return "redirect:/" + storeId + "/details";
    }

    @GetMapping("/{storeId}/edit/{clerkId}")
    public String editClerk(@PathVariable Integer clerkId, @PathVariable Integer storeId, Model schaufenster) {
        schaufenster.addAttribute("clerk", new ClerkDTO(clerkService.getClerk(clerkId).getName()));
        schaufenster.addAttribute("clerkId", clerkId);
        schaufenster.addAttribute("storeId", storeId);
        return "/edit";
    }

    @PostMapping("/{storeId}/edit/{clerkId}")
    public String editClerk(@PathVariable Integer clerkId, @PathVariable Integer storeId, @ModelAttribute(value = "clerk") ClerkDTO clerkDTO) {
        clerkService.edit(clerkDTO, clerkId);
        return "redirect:/" + storeId + "/details";
    }

    @PostMapping("/{storeId}/delete/{clerkId}")
    public String deleteClerk(@PathVariable Integer clerkId, @PathVariable Integer storeId) {
        clerkService.delete(clerkId);
        return "redirect:/" + storeId + "/details";
    }

}
