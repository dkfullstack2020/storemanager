package awacademy.storemanager.Store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.spec.OAEPParameterSpec;
import java.util.List;
import java.util.Optional;

@Service
public class StoreService {
    private StoreRepo storeRepo;

    @Autowired
    public StoreService(StoreRepo storeRepo) {
        this.storeRepo = storeRepo;
    }
//todo

    public List<Store> getStoreList() {
        return storeRepo.findAll();
    }

    public void add(StoreDTO storeDTO) {
        Store store = new Store(storeDTO.getName(), storeDTO.getAddress());
        storeRepo.save(store);
    }

    public Store getStore(Integer storeID) {
        Optional<Store> store = storeRepo.findById(storeID);
        return store.get();
    }


}
