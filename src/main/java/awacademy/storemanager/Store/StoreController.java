package awacademy.storemanager.Store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StoreController {
    private StoreService storeService;

    @Autowired
    StoreController(StoreService storeService) {this.storeService = storeService;}

    @GetMapping("/welcome")
    public String home(Model schaufenster){
        schaufenster.addAttribute("storeList", storeService.getStoreList());
        schaufenster.addAttribute("store", new StoreDTO("", ""));
        return ("welcome");

    }
    @PostMapping("/welcome")
    public String addStore(@ModelAttribute(value = "store") StoreDTO storeDTO) {
        storeService.add(storeDTO);

        return "redirect:/welcome";
    }




}
