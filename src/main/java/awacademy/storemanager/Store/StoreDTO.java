package awacademy.storemanager.Store;

import javax.validation.constraints.NotEmpty;

public class StoreDTO {

    @NotEmpty
    private String name;
private String address;

    public StoreDTO(@NotEmpty String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
