package awacademy.storemanager.Store;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StoreRepo extends CrudRepository<Store, Integer> {
List<Store> findAll();

}
